import 'package:flutter/material.dart';
import 'package:flutter_fullscreen/flutter_fullscreen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fullscreen Example',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    final fullscreen = Fullscreen.instance.enabled;

    return Scaffold(
      appBar: AppBar(title: const Text('Fullscreen example')),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Fullscreen is: ${fullscreen ? 'ON' : 'OFF'}"),
            TextButton(onPressed: _toggle, child: const Text('Toggle'))
          ],
        ),
      ),
    );
  }

  void _toggle() {
    Fullscreen.instance.toggleFullsreen();
    setState(() {});
  }
}

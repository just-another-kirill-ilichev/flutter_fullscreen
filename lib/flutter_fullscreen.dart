library flutter_fullscreen;

import './src/fullscreen_delegate_base.dart';

import './src/fullscreen_delegate_mobile.dart'
    if (dart.library.js) 'src/fullscreen_delegate_web.dart';

class Fullscreen {
  static Fullscreen? _instance;

  static Fullscreen get instance {
    _instance ??= Fullscreen._();
    return _instance!;
  }

  Fullscreen._();

  final FullscreenDelegateBase _delegate = FullscreenDelegate();

  bool _enabled = false;

  bool get enabled => _enabled;

  Future<void> setFullscreen(bool value) async {
    await _delegate.setFullscreen(value);
    _enabled = value;
  }

  Future<void> toggleFullsreen() async {
    await setFullscreen(!_enabled);
  }
}

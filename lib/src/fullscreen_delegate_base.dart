abstract class FullscreenDelegateBase {
  Future<void> setFullscreen(bool value);
}

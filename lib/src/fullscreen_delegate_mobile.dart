import 'package:flutter/services.dart';
import 'fullscreen_delegate_base.dart';

class FullscreenDelegate implements FullscreenDelegateBase {
  @override
  Future<void> setFullscreen(bool value) async {
    if (value) {
      await SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersiveSticky);
    } else {
      await SystemChrome.setEnabledSystemUIMode(
        SystemUiMode.manual,
        overlays: SystemUiOverlay.values,
      );
    }
  }
}

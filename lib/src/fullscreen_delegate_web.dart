// ignore: avoid_web_libraries_in_flutter
import 'dart:html' as html;
import 'fullscreen_delegate_base.dart';

class FullscreenDelegate implements FullscreenDelegateBase {
  @override
  Future<void> setFullscreen(bool value) async {
    if (value) {
      html.document.documentElement?.requestFullscreen();
    } else {
      html.document.exitFullscreen();
    }
  }
}

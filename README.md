## flutter_fullscreen

This package helps to easily enable/disable fullscreen mode on iOS, Android and Web platforms.